package kr.co.heystars.api.course.model;

import kr.co.heystars.api.mediaServer.model.MediaModel;
import lombok.Data;

import java.util.List;

@Data
public class ChScenarioModel {

    private Long id;
    private boolean isIntro;
    private boolean isStudy;
    private boolean isQuiz;
    private String answerKor;
    private String answer;
    private String responseKor;
    private String response;
    private Long orderIndex;
    private Long subOrderIndex;
    private String question;
    private List<MediaModel> mediaModels;
    private List<ComScenarioModel> comModelAnswers;
    private ComScenarioModel comModelResponse;
}
