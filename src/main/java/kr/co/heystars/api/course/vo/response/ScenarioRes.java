package kr.co.heystars.api.course.vo.response;

import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.course.domain.ComScenario;
import kr.co.heystars.api.course.model.ChScenarioModel;
import lombok.Data;

import java.util.List;

@Data
public class ScenarioRes {

    List<List<IntroScenario>> introScenariosList;
    List<List<StudyScenario>> studyScenarioList;
    List<List<QuizScenario>> quizScenarioList;
}
