package kr.co.heystars.api.course.vo.response;

import lombok.Data;

@Data
public class StudyScenario {
    String answerKor;
    String answer;
    String responseKor;
    String response;
 //   String imgUrl;
    String orderIndex;
    String subOrderIndex;
    String mediaUrl;
}
