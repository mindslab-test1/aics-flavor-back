package kr.co.heystars.api.course.common;

public interface Language {
    int KOR = 0;
    int ENG = 1;
    int VIET = 2;
    int INDO = 3;
}
