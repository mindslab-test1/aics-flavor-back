package kr.co.heystars.api.course.domain;

import kr.co.heystars.api.course.model.ChScenarioModel;
import kr.co.heystars.api.course.model.ComScenarioModel;
import kr.co.heystars.api.mediaServer.domain.Media;
import kr.co.heystars.api.mediaServer.model.MediaModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "CH_SCENARIO")
public class ChScenario {

    @Id
    @SequenceGenerator(name = "CH_SCENARIO_SEQ_GEN", sequenceName = "CH_SCENARIO_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CH_SCENARIO_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "IS_INTRO", nullable = false)
    private boolean isIntro;

    @Column(name = "IS_STUDY", nullable = false)
    private boolean isStudy;

    @Column(name = "IS_QUIZ", nullable = false)
    private boolean isQuiz;

    @Column(name ="ANSWER_KOR")
    private String answerKor;

    @Column(name = "ANSWER", nullable = true, columnDefinition = "varchar2(1500)")
    private String answer;

    @Column(name = "RESPONSE_KOR")
    private String responseKor;

    @Column(name = "RESPONSE", nullable = true, columnDefinition = "varchar2(1500)")
    private String response;

    @Column(name = "ORDER_INDEX", nullable = true)
    private Long orderIndex;

    @Column(name = "SUB_ORDER_INDEX")
    private Long subOrderIndex;

    @Column(name = "QUESTION")
    private String question;

    @OneToMany(mappedBy = "chScenario")
    private List<Media> medias = new ArrayList<>();

    @ManyToOne
    private Chapter chapter;

    public ChScenarioModel toModel(){

        ChScenarioModel chScenarioModel = new ChScenarioModel();
        chScenarioModel.setId(this.id);
        chScenarioModel.setIntro(this.isIntro);
        chScenarioModel.setStudy(this.isStudy);
        chScenarioModel.setQuiz(this.isQuiz);
        chScenarioModel.setAnswerKor(this.answerKor);
        chScenarioModel.setAnswer(this.answer);
        chScenarioModel.setResponseKor(this.responseKor);
        chScenarioModel.setResponse(this.response);
        chScenarioModel.setOrderIndex(this.orderIndex);
        chScenarioModel.setSubOrderIndex(this.subOrderIndex);
        chScenarioModel.setQuestion(this.question);

        List<MediaModel> mediaModels = new LinkedList<>();
        if(this.medias != null) {
            this.medias.forEach(value -> {mediaModels.add(value.toModel(false));});
        }

        chScenarioModel.setMediaModels(mediaModels);

        return chScenarioModel;
    }
}
