package kr.co.heystars.api.course.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.course.domain.Chapter;
import kr.co.heystars.api.course.domain.ComScenario;
import kr.co.heystars.api.course.domain.Word;
import kr.co.heystars.api.course.model.ChScenarioModel;
import kr.co.heystars.api.course.model.ComScenarioModel;
import kr.co.heystars.api.course.model.WordModel;
import kr.co.heystars.api.course.repository.ChScenarioRepository;
import kr.co.heystars.api.course.repository.ChapterRepository;
import kr.co.heystars.api.course.repository.ComScenarioRepository;
import kr.co.heystars.api.course.common.Language;
import kr.co.heystars.api.course.repository.WordRepository;
import kr.co.heystars.api.course.vo.response.*;
import kr.co.heystars.api.exception.InternalServerException;
import kr.co.heystars.api.mediaServer.domain.Media;
import kr.co.heystars.api.mediaServer.model.MediaModel;
import kr.co.heystars.api.mediaServer.service.MediaServerService;
import kr.co.heystars.api.tutor.repository.TutorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

enum StudyStep {
    INTRO, STUDY, QUIZ
}


@Slf4j
@Service
public class CourseService {

    @Autowired
    ChScenarioRepository chScenarioRepository;

    @Autowired
    ChapterRepository chapterRepository;

    @Autowired
    TutorRepository tutorRepository;

    @Autowired
    ComScenarioRepository comScenarioRepository;

    @Autowired
    MediaServerService mediaServerService;

    @Autowired
    WordRepository wordRepository;

    static final String SPLIT = "@@";
    int language;
    int chapterId;


    public List<Chapter> getChapters(){

        try{
            return chapterRepository.findAll();
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "CourseService.getChapters", e.getMessage());
        }
    }

    public TutorAndChapterRes getTutorsAndChapters(){

        try{
            TutorAndChapterRes tutorAndChapterRes = new TutorAndChapterRes();
            tutorAndChapterRes.setTutors(tutorRepository.findAll());
            tutorAndChapterRes.setChapters(chapterRepository.findAll());

            return tutorAndChapterRes;
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "CourseService.getTutorsAndChapters", e.getMessage());
        }
    }

    public ScenarioRes getChScenario(String chapterEng, String lang){

        ScenarioRes scenarioRes = new ScenarioRes();
        Chapter chapter = chapterRepository.findBytitleEng(chapterEng);

        chapterId = chapter.getId().intValue();

        if("Viet".equals(lang)) language = Language.VIET;
        else if("Indo".equals(lang)) language = Language.INDO;
        else language = Language.ENG;

        //Change common scenario entity to model
        List<ComScenario> comScenarioList = comScenarioRepository.findAll();
        List<ComScenarioModel> comScenarioModelList = new LinkedList<>();
        comScenarioList.forEach(value ->{ comScenarioModelList.add(value.toModel());});

        //Change chapter scenario entity to model
        List<ChScenario> chScenarioList = chScenarioRepository.getChapterScenario(chapter);
        List<ChScenarioModel> chScenarioModelList = new LinkedList<>();
        chScenarioList.forEach(value -> {chScenarioModelList.add(value.toModel());});

        scenarioRes.setIntroScenariosList(getIntroScenario(chScenarioModelList, comScenarioModelList));

        scenarioRes.setStudyScenarioList(getStudyScenario(chScenarioModelList, comScenarioModelList));

        scenarioRes.setQuizScenarioList(getQuizScenario(chScenarioModelList, comScenarioModelList));

        return scenarioRes;
    }

    List getIntroScenario(List<ChScenarioModel> chScenarioModelList, List<ComScenarioModel> comScenarioModelList){

        Stream<ChScenarioModel> chScenarioModelIntro = chScenarioModelList.stream().filter(chScenarioModel -> chScenarioModel.isIntro() == true);

        //intro에 들어있는 common scenario에 값을 넣어준 후
        //해당 tutor에 해당하는 media값을 넣어 줌
        List<ChScenarioModel> chScenarioModels = chScenarioModelIntro.map(value -> {

            Stream<ComScenarioModel> modelStreamAnswer = comScenarioModelList.stream();

            List<ComScenarioModel> answer = modelStreamAnswer
                    .filter( comScenarioModel -> (comScenarioModel.getDivision().equals(value.getAnswer())))
                    .map(comScenarioModel -> {
                        Optional<MediaModel> media = comScenarioModel.getMediaModels().stream()
                                .filter(mediaModel -> (mediaModel.getTutorID() == 1)).findFirst();

                        comScenarioModel.setMediaModel(media.orElse(null));
                        return comScenarioModel;
                    })
                    .collect(Collectors.toList());


            value.setComModelAnswers(answer);

            Optional<ComScenarioModel> commonModelRes = comScenarioModelList.stream()
                    .filter(comScenarioModel -> (comScenarioModel.getDivision().equals(value.getResponse())))
                    .findFirst();

            if(commonModelRes.isPresent()) {
                ComScenarioModel commonRes = commonModelRes.get();
                commonRes.setMediaModel(commonRes.getMediaModels().stream().filter(mediaModel -> (mediaModel.getTutorID() == 1)).findFirst().orElse(null));
                value.setComModelResponse(commonRes);
            }

            return value;
        }).collect(Collectors.toList());


        return insertScenarioRes(chScenarioModels, StudyStep.INTRO);
    }


    List getQuizScenario(List<ChScenarioModel> chScenarioModelList, List<ComScenarioModel> comScenarioModelList){

        List<ChScenarioModel> chScenarioModels = chScenarioModelList.stream()
                .filter(chScenarioModel -> (chScenarioModel.isQuiz() == true))
                .map(chScenarioModel -> {
                    chScenarioModel.setAnswerKor(chScenarioModel.getAnswer());
                    return chScenarioModel;})
                .map(chScenarioModel -> setScenarioResponseAnswer(chScenarioModel, comScenarioModelList))
                .collect(Collectors.toList());

        return insertScenarioRes(chScenarioModels, StudyStep.QUIZ);
    }

    List getStudyScenario(List<ChScenarioModel> chScenarioModelList, List<ComScenarioModel> comScenarioModelList){

        List<ChScenarioModel> chScenarioModels = chScenarioModelList.stream()
                .filter(chScenarioModel -> chScenarioModel.isStudy() == true)
                .map(chScenarioModel -> setScenarioResponseAnswer(chScenarioModel, comScenarioModelList))
                .collect(Collectors.toList());

        List scenarioResList = insertScenarioRes(chScenarioModels, StudyStep.STUDY);

        return scenarioResList;
    }


    /*
     * study와 quiz chapter scenario에 들어있는
     * Common scenario들에 데이터를 넣어줌
     * */
    public ChScenarioModel setScenarioResponseAnswer(ChScenarioModel chScenarioModel, List<ComScenarioModel> comScenarioModelList){

        comScenarioModelList.stream().forEach(value -> {
            if(chScenarioModel.getAnswer() != null) {
                if (chScenarioModel.getAnswer().equals(value.getDivision())) {
                    chScenarioModel.setAnswer(value.getEngText());
                    chScenarioModel.setAnswerKor(value.getKorText());
                }else if(chScenarioModel.getAnswer().split(SPLIT).length>1){
                    String[] lang = chScenarioModel.getAnswer().split(SPLIT);
                    chScenarioModel.setAnswer(lang[language]);
                    chScenarioModel.setAnswerKor(lang[Language.KOR]);
                }
            }

            if(chScenarioModel.getResponse() != null){
                if (chScenarioModel.getResponse().equals(value.getDivision())) {
                    chScenarioModel.setResponse(value.getEngText());
                    chScenarioModel.setResponseKor(value.getKorText());
                }else if(chScenarioModel.getResponse().split(SPLIT).length>1){
                    String[] lang = chScenarioModel.getResponse().split(SPLIT);
                    chScenarioModel.setResponse(lang[language]);
                    chScenarioModel.setResponseKor(lang[Language.KOR]);
                }
            }
        });

        return chScenarioModel;
    }

    public List insertScenarioRes(List<ChScenarioModel> chScenarioModels, StudyStep studyStep){

        Long currentOrderIndex = chScenarioModels.get(0).getOrderIndex();
        List chScenarioModelSubList;
        List chScenarioModelListList;
        List<WordModel> wordModelList = new LinkedList<>();

        if(studyStep == StudyStep.QUIZ || studyStep == StudyStep.STUDY) {

            if(studyStep == StudyStep.QUIZ ) {
                chScenarioModelSubList = new LinkedList<QuizScenario>();
                chScenarioModelListList = new LinkedList<List<QuizScenario>>();
            }else {
                chScenarioModelSubList = new LinkedList<StudyScenario>();
                chScenarioModelListList = new LinkedList<List<StudyScenario>>();
                List<Word> wordList = wordRepository.findAllByChapterId(chapterId);
                wordList.forEach(word -> wordModelList.add(word.toModel()));
            }

        } else {
            chScenarioModelSubList = new LinkedList<IntroScenario>();
            chScenarioModelListList = new LinkedList<List<IntroScenario>>();
        }

        for (int i = 0; i < chScenarioModels.size(); i++) {

            if(i == chScenarioModels.size()-1){
                chScenarioModelSubList.add(setMediaAndWordToObject(chScenarioModels.get(i), studyStep, wordModelList));
                chScenarioModelListList.add(chScenarioModelSubList);
            }
            else if (currentOrderIndex.equals(chScenarioModels.get(i).getOrderIndex())) {
                chScenarioModelSubList.add(setMediaAndWordToObject(chScenarioModels.get(i), studyStep, wordModelList));
            } else {
                currentOrderIndex = chScenarioModels.get(i).getOrderIndex();
                chScenarioModelListList.add(chScenarioModelSubList);
                chScenarioModelSubList = new LinkedList<>();
                chScenarioModelSubList.add(setMediaAndWordToObject(chScenarioModels.get(i), studyStep, wordModelList));
            }
        }

        return chScenarioModelListList;
    }

    public Object setMediaAndWordToObject(ChScenarioModel chScenarioModel, StudyStep studyStep, List<WordModel> wordModelList){

        ObjectMapper objectMapper = new ObjectMapper();
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(objectMapper.convertValue(chScenarioModel, Map.class));

        if(StudyStep.QUIZ == studyStep || StudyStep.STUDY == studyStep){

            Optional<MediaModel> mediaFilter = (chScenarioModel != null)
                    ? chScenarioModel.getMediaModels()
                    .stream()
                    .filter( mediaModel -> (mediaModel.getTutorID() == 1 || mediaModel.getTutorID() == 0)).findFirst()
                    : null;

            if(StudyStep.QUIZ == studyStep) {
                QuizScenario quizScenario = gson.fromJson(jsonElement, QuizScenario.class);

                if (mediaFilter != null && mediaFilter.isPresent())
                    quizScenario.setMediaUrl(mediaFilter.get().getUrl());

                return quizScenario;
            }else if(StudyStep.STUDY == studyStep){
                StudyScenario studyScenario = gson.fromJson(jsonElement, StudyScenario.class);

                if(mediaFilter != null && mediaFilter.isPresent())
                    studyScenario.setMediaUrl(mediaFilter.get().getUrl());

                Optional<WordModel> wordFilter = (wordModelList.size()> 0)
                        ?wordModelList.stream().filter(wordModel -> (wordModel.getWord().equals(studyScenario.getAnswer()))).findFirst()
                        :null;

                if(wordFilter != null && wordFilter.isPresent()){
                    WordModel filteredWord = wordFilter.get();
                    studyScenario.setAnswerKor(filteredWord.getWord());

                    if(language == Language.VIET) studyScenario.setAnswer(filteredWord.getWordViet());
                    else if(language == Language.INDO) studyScenario.setAnswer(filteredWord.getWordINDO());
                    else studyScenario.setAnswer(filteredWord.getWordEng());
                }

                return studyScenario;
            }
        }else{
            IntroScenario introScenario = gson.fromJson(jsonElement, IntroScenario.class);
            return introScenario;
        }

        return null;
    }

    public void insertCourseStep(MultipartFile file, String answer, Integer isIntro,
                                 Integer isStudy, Integer isQuiz, String response,
                                 String chapterEng, Integer orderIndx, Integer subOrderIndex,
                                 Integer tutorId, String type){

        ChScenario chScenario = new ChScenario();
        chScenario.setAnswer(answer);
        chScenario.setIntro(isIntro == 1);
        chScenario.setStudy(isStudy == 1);
        chScenario.setQuiz(isQuiz == 1);
        chScenario.setResponse(response);
        chScenario.setChapter(chapterRepository.findBytitleEng(chapterEng));
        chScenario.setOrderIndex(Integer.toUnsignedLong(orderIndx));
        chScenario.setSubOrderIndex(Integer.toUnsignedLong(subOrderIndex));

        ChScenario savedChScenario = chScenarioRepository.save(chScenario);


        if(file != null) {
            Media media = mediaServerService.uploadMedia(file, type, tutorId, savedChScenario);

        }
    }
}

