package kr.co.heystars.api.course.repository;

import kr.co.heystars.api.course.domain.ComScenario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComScenarioRepository extends JpaRepository<ComScenario, Long> {

    ComScenario findByDivisionAndAndOrderIndex(String division, int orderIndex);
}
