package kr.co.heystars.api.course.domain;

import kr.co.heystars.api.course.model.ChapterModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "CHAPTER")
public class Chapter {

    @Id
    @SequenceGenerator(name = "CHAPTER_SEQ_GEN", sequenceName = "CHAPTER_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CHAPTER_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "TITLE_KOR")
    private String titleKor;

    @Column(name = "TITLE_ENG")
    private String titleEng;

    @Column(name = "LEVEL_NUM")
    private Integer levelNum;

    @Column(name = "IMG_URL")
    private String imgUrl;


    public ChapterModel toModel(){
        ChapterModel chapterModel = new ChapterModel();
        chapterModel.setId(this.id);
        chapterModel.setTitleKor(this.titleKor);
        chapterModel.setTitleEng(this.titleEng);
        chapterModel.setLevelNum(this.levelNum);
        chapterModel.setImgUrl(this.imgUrl);

        return  chapterModel;
    }
}
