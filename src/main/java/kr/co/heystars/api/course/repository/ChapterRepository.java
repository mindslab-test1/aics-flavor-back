package kr.co.heystars.api.course.repository;

import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.course.domain.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ChapterRepository extends JpaRepository<Chapter, Long> {

    Chapter findBytitleEng(String titleEng);
}
