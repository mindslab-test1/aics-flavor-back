package kr.co.heystars.api.course.vo.response;

import kr.co.heystars.api.course.domain.ComScenario;
import kr.co.heystars.api.course.model.ComScenarioModel;
import lombok.Data;

import java.util.List;

@Data
public class IntroScenario {
    String answerKor;
    String answer;
    String responseKor;
    String response;
    String orderIndex;
    String subOrderIndex;
    //String mediaUrl;
    List<ComScenarioModel> comModelAnswers;
    private ComScenarioModel comModelResponse;
}
