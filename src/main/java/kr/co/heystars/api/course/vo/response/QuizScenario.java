package kr.co.heystars.api.course.vo.response;

import lombok.Data;

@Data
public class QuizScenario {
    String answerKor;
    String answer;
    String responseKor;
    String response;
    String orderIndex;
    String subOrderIndex;
    String mediaUrl;
    String question;
}
