package kr.co.heystars.api.course.domain;

import kr.co.heystars.api.course.model.WordModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "WORD")
public class Word {

    @Id
    @SequenceGenerator(name = "WORD_SEQ_GEN", sequenceName = "WORD_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "WORD_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "WORD", nullable = false)
    private String word;

    @Column(name = "SYMBOL")
    private String symbol;

    @Column(name = "WORD_ENG")
    private String wordEng;

    @Column(name = "WORD_VIET")
    private String wordViet;

    @Column(name = "WORD_INDO")
    private String wordINDO;

    @Column(name = "ChapterId")
    private int chapterId;

    public WordModel toModel(){
        WordModel wordModel = new WordModel();
        wordModel.setId(this.id);
        wordModel.setWord(this.word);
        wordModel.setSymbol(this.symbol);
        wordModel.setWordEng(this.wordEng);
        wordModel.setWordViet(this.wordViet);
        wordModel.setWordINDO(this.wordINDO);
        wordModel.setChapterId(this.chapterId);

        return wordModel;
    }
}
