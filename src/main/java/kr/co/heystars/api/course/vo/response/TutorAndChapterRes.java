package kr.co.heystars.api.course.vo.response;

import kr.co.heystars.api.course.domain.Chapter;
import kr.co.heystars.api.tutor.domain.Tutor;
import lombok.Data;

import java.util.List;

@Data
public class TutorAndChapterRes {

    List<Tutor> tutors;
    List<Chapter> chapters;
}
