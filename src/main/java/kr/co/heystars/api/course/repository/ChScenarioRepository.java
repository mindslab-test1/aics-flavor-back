package kr.co.heystars.api.course.repository;

import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.course.domain.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ChScenarioRepository extends JpaRepository<ChScenario, Long> {

    @Query("select c from ChScenario c where c.chapter = ?1 order by c.orderIndex, c.subOrderIndex ")
    List<ChScenario> getChapterScenario(Chapter chapter);

    @Query("select c from ChScenario c where c.id = ?1")
    ChScenario getChScenarioById(Long id);
    //List<ChScenario> findByChapter(Chapter chapter);
}
