package kr.co.heystars.api.course.model;

import lombok.Data;

@Data
public class ChapterModel {

    private Long id;
    private String titleKor;
    private String titleEng;
    private Integer levelNum;
    private String imgUrl;

}
