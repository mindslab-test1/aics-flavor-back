package kr.co.heystars.api.course.domain;

import kr.co.heystars.api.course.model.ComScenarioModel;
import kr.co.heystars.api.mediaServer.domain.Media;
import kr.co.heystars.api.mediaServer.model.MediaModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
@Entity
@Builder
@Table(name = "COM_SCENARIO")
public class ComScenario {

    @Id
    @SequenceGenerator(name = "COM_SCENARIO_SEQ_GEN", sequenceName = "COM_SCENARIO_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COM_SCENARIO_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "DIVISION", nullable = false)
    private String division;

    @Column(name = "KOR_TEXT")
    private String korText;

    @Column(name = "ENG_TEXT")
    private String engText;

    @Column(name = "VIET_TEXT")
    private String vietTExt;

    @Column(name = "INDO_TEXT")
    private String indoText;

    @Column(name = "ORDER_INDEX")
    private int orderIndex;

    @Column(name = "IMG_URL")
    private String imgUrl;

    @OneToMany(mappedBy = "comScenario")
    private List<Media> medias = new ArrayList<>();

    public ComScenarioModel toModel(){

        ComScenarioModel comScenarioModel = new ComScenarioModel();
        comScenarioModel.setId(this.id);
        comScenarioModel.setDivision(this.division);
        comScenarioModel.setKorText(this.korText);
        comScenarioModel.setEngText(this.engText);
        comScenarioModel.setVietTExt(this.vietTExt);
        comScenarioModel.setIndoText(this.indoText);
        comScenarioModel.setOrderIndex(this.orderIndex);
        comScenarioModel.setImgUrl(this.imgUrl);

        List<MediaModel> mediaModels = new LinkedList<>();
        if(medias != null) this.medias.forEach(value -> { mediaModels.add(value.toModel(false)); });

        comScenarioModel.setMediaModels(mediaModels);

        return comScenarioModel;
    }

    @Override
    public String toString(){
        return "id:"+id+", division:"+division+", korText:"+korText
                +", engText:"+engText+", vietText"+vietTExt+", indoText:"+indoText+", orderIndex:"+orderIndex;
    }
}
