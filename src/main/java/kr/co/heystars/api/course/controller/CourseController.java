package kr.co.heystars.api.course.controller;

import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.course.model.ChScenarioModel;
import kr.co.heystars.api.course.service.CourseService;
import kr.co.heystars.api.course.vo.response.ScenarioRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @ResponseBody
    @GetMapping(value = "/getScenario/{chapter}/{language}")
    public ScenarioRes list(@PathVariable String chapter, @PathVariable String language) {

        return courseService.getChScenario(chapter, language);
    }

    @ResponseBody
    @GetMapping(value = "/getChapters")
    public ResponseEntity getChapters(){
        return new ResponseEntity(courseService.getChapters(), HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/getTutorsAndChapters")
    public ResponseEntity getTutorsAndCourse(){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(courseService.getTutorsAndChapters(),resHeaders, HttpStatus.OK);
    }

    @PostMapping(value = "/insertCourseStep")
    public void insertCourseStep(@RequestParam(required = false) MultipartFile file,
                                 @RequestParam(required = false) String answer,
                                 @RequestParam(required = false) Integer isIntro,
                                 @RequestParam(required = false) Integer isStudy,
                                 @RequestParam(required = false) Integer isQuiz,
                                 @RequestParam(required = false) String response,
                                 @RequestParam(required = false) String chapterEng,
                                 @RequestParam(required = false) Integer orderIndx,
                                 @RequestParam(required = false) Integer subOrderIndex,
                                 @RequestParam(required = false) Integer tutorId,
                                 @RequestParam(required = false) String type
                                 ){

        courseService.insertCourseStep(file, answer, isIntro, isStudy,
                isQuiz, response, chapterEng, orderIndx, subOrderIndex, tutorId, type);
    }
}
