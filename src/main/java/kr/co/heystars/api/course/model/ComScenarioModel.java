package kr.co.heystars.api.course.model;

import kr.co.heystars.api.mediaServer.domain.Media;
import kr.co.heystars.api.course.domain.ComScenario;
import kr.co.heystars.api.mediaServer.model.MediaModel;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
public class ComScenarioModel {

    private Long id;
    private String division;
    private String korText;
    private String engText;
    private String vietTExt;
    private String indoText;
    private int orderIndex;
    private String imgUrl;
    private MediaModel mediaModel;
    private List<MediaModel> mediaModels = new ArrayList<>();

}
