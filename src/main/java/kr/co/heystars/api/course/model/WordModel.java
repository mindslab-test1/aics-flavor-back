package kr.co.heystars.api.course.model;

import lombok.Data;

@Data
public class WordModel {

    private Long id;
    private String word;
    private String symbol;
    private String wordEng;
    private String wordViet;
    private String wordINDO;
    private int chapterId;
}
