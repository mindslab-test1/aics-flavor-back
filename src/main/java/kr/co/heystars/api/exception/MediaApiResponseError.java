package kr.co.heystars.api.exception;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class MediaApiResponseError {
    private int statusCode;
    private String code;
    private String message;
}
