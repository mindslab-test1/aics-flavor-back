package kr.co.heystars.api.mediaServer.model;

import kr.co.heystars.api.course.domain.ComScenario;
import kr.co.heystars.api.course.model.ComScenarioModel;
import lombok.Data;

import javax.persistence.*;

@Data
public class MediaModel {

    private Long id;
    private String name;
    private String type;
    private String url;
    private int tutorID;
    private ComScenarioModel comScenarioModel;
}
