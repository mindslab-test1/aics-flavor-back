package kr.co.heystars.api.mediaServer.vo.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MediaUploadRes {
    private String filename;
    private String fileDownloadUri;
    private String fileType;
    private long fileSize;
}
