package kr.co.heystars.api.mediaServer.controller;

import kr.co.heystars.api.mediaServer.service.MediaServerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RequestMapping("/mediaServer")
@RestController
public class MediaServerController {

    @Autowired
    MediaServerService mediaServerService;

    @PostMapping("/common/scenario:upload")
    public String upload(@RequestParam("file") MultipartFile file){

        log.info("파일 이름 : "+file.getOriginalFilename());
        log.info("파일 크기 : "+file.getSize());


        return mediaServerService.uploadMediaGetUrl(file);
    }

    @PostMapping("/image/chapterImage:upload")
    public String upload(@RequestParam("file") MultipartFile file,
                       @RequestParam int chapterId ){

        return mediaServerService.uploadImage(file, chapterId);
    }
}
