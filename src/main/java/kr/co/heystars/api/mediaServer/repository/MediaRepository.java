package kr.co.heystars.api.mediaServer.repository;

import kr.co.heystars.api.mediaServer.domain.Media;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository extends JpaRepository<Media, Long> {

}
