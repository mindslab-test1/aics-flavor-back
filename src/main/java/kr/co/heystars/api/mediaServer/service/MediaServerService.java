package kr.co.heystars.api.mediaServer.service;

import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.course.domain.Chapter;
import kr.co.heystars.api.course.domain.ComScenario;
import kr.co.heystars.api.course.repository.ComScenarioRepository;
import kr.co.heystars.api.exception.InternalServerException;
import kr.co.heystars.api.mediaServer.domain.Media;
import kr.co.heystars.api.mediaServer.repository.MediaRepository;
import kr.co.heystars.api.mediaServer.vo.response.MediaUploadRes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class MediaServerService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    MediaRepository mediaRepository;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;

    @Value("${media.server.port}")
    private String MEDIA_SERVER_PORT;

    @Value("${media.server.dir}")
    private String MEDIA_SERVER_DIR;

    public Media uploadMedia(MultipartFile file, String type, int tutorId, ChScenario chScenario){

        try{

            MediaUploadRes mediaUploadRes = uploadFile(file);

            Media media = new Media();
            media.setName(file.getOriginalFilename());
            media.setType(type);
            media.setTutorID(tutorId);
            media.setUrl(mediaUploadRes.getFileDownloadUri());
            media.setChScenario(chScenario);
            Media savedMedia = mediaRepository.save(media);

            return savedMedia;
            //return null;
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "MediaServerService.uploadMedia", e.getMessage());
        }
    }

    public String uploadMediaGetUrl(MultipartFile file){
        try{
            MediaUploadRes mediaUploadRes = uploadFile(file);

            return mediaUploadRes.getFileDownloadUri();

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public String uploadImage(MultipartFile file, int chapterId){
        MediaUploadRes mediaUploadRes = new MediaUploadRes();
        try{
            mediaUploadRes = uploadFile(file);
            log.info("media server image url : {}", mediaUploadRes.getFileDownloadUri());

        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "MediaServerService.uploadImage", e.getMessage());
        }

        return mediaUploadRes.getFileDownloadUri();
    }

    public MediaUploadRes uploadFile(MultipartFile file){
        final URI uploadUri = URI.create(MEDIA_SERVER_URL + "/media/file:upload");

        try{
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", file.getResource());
            body.add("uploadDirectory", MEDIA_SERVER_DIR);

            log.info("### file: {}, uploadDirectory: {}", file.getName(), MEDIA_SERVER_DIR);

            HttpEntity<MultiValueMap<String, Object>> requestHttpEntity = new HttpEntity<>(body, requestHeaders);
            ResponseEntity<MediaUploadRes> response = restTemplate.exchange(uploadUri, HttpMethod.POST,
                    requestHttpEntity, new ParameterizedTypeReference<MediaUploadRes>() {});

            if(response.getStatusCode().value() != 201){
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "MediaServerService.uploadMedia", "StatusCode : "+response.getStatusCode().toString());
            }

            return response.getBody();
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "MediaServerService.uploadMedia", e.getMessage());
        }
    }

    public String deleteMedia(String url){

        URI deleteUri = URI.create(MEDIA_SERVER_URL + "/media/file:delete");
        String mediaUrl = url.split("/media")[1];

        try{
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);

            Map<String, Object> body = new HashMap<>();
            body.put("deleteFileFullPath", mediaUrl);

            HttpEntity<Map<String, Object>> requestHttpEntity = new HttpEntity<>(body, requestHeaders);
            ResponseEntity<String> response = restTemplate.exchange(deleteUri, HttpMethod.DELETE, requestHttpEntity, String.class);

            return response.getBody();

        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "MediaServerService.deleteMedia", e.getMessage());
        }
    }

}
