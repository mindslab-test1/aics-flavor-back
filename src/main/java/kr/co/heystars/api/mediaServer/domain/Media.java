package kr.co.heystars.api.mediaServer.domain;

import kr.co.heystars.api.course.domain.ChScenario;
import kr.co.heystars.api.mediaServer.model.MediaModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import kr.co.heystars.api.course.domain.ComScenario;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "MEDIA")
public class Media {

    @Id
    @SequenceGenerator(name = "MEDIA_SEQ_GEN", sequenceName = "MEDIA_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEDIA_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "URL")
    private String url;

    @Column(name = "TUTOR_ID")
    private int tutorID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COM_SCENARIO_ID")
    private ComScenario comScenario;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CH_SCENARIO_ID")
    private ChScenario chScenario;

    public MediaModel toModel(boolean includeAssociations){
        MediaModel mediaModel = new MediaModel();
        mediaModel.setId(this.id);
        mediaModel.setName(this.name);
        mediaModel.setType(this.type);
        mediaModel.setUrl(this.url);
        mediaModel.setTutorID(this.tutorID);
        if(includeAssociations) mediaModel.setComScenarioModel(this.comScenario.toModel());
        else mediaModel.setComScenarioModel(null);

        return mediaModel;
    }

    @Override
    public String toString() {
        return "Media{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", tutorID=" + tutorID +
                ", comScenario=" + comScenario +
                '}';
    }
}
