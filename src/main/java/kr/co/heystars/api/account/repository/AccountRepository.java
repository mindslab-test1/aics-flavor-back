package kr.co.heystars.api.account.repository;

import kr.co.heystars.api.account.domain.HSAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<HSAccount, Long> {
    HSAccount findByemail(String email);
}
