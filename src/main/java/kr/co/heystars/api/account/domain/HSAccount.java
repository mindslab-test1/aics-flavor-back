package kr.co.heystars.api.account.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "HS_ACCOUNT")
public class HSAccount {

    @Id
    @SequenceGenerator(name = "HS_ACCOUNT_SEQ_GEN", sequenceName = "HS_ACCOUNT_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HS_ACCOUNT_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "NICKNAME", nullable = true)
    private String nickname;

    @Column(name = "BIRTH", nullable = true)
    private String birth;

    @Column(name = "SEX", nullable = true)
    private String sex;

    @Column(name = "LANG", nullable = true)
    private String lang;

    @Column(name = "SUBSCRIBE", updatable = false)
    @ColumnDefault("0")
    private int subscribe;

    @Override
    public String toString(){
        return "id:"+id+", email:"+email+", nickname:"+nickname+", birth:"+birth+", sex:"+sex+", lang:"+lang+", subscribe:"+subscribe;
    }
}
