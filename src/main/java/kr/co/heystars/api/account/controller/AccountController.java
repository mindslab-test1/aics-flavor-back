package kr.co.heystars.api.account.controller;

import kr.co.heystars.api.account.domain.HSAccount;
import kr.co.heystars.api.account.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping(value = "account")
@RestController
public class AccountController {

    @Autowired
    AuthService authService;

    @GetMapping(value = "getUserInfo")
    public ResponseEntity<HSAccount> getUserInfo(@RequestHeader HttpHeaders httpHeaders){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");
        log.info(httpHeaders.toString());
        return new ResponseEntity(authService.getUserInfo(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION)), resHeaders, HttpStatus.OK);
    }

    @PostMapping(value = "saveUserInfo")
    public ResponseEntity saveUserInfo(@RequestHeader HttpHeaders httpHeaders, @RequestBody HSAccount HSAccount){

        return new ResponseEntity(authService.saveUserInfo(HSAccount));
    }

    // user lang 수정
    @PostMapping(value ="putUserLang")
    public ResponseEntity putUserLang(@RequestHeader HttpHeaders httpHeaders, @RequestBody HSAccount HSAccount){
        return new ResponseEntity(authService.putUserLang(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION), HSAccount));
    }


}
