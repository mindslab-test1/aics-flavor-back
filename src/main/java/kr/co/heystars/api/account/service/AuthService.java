package kr.co.heystars.api.account.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import kr.co.heystars.api.account.domain.HSAccount;
import kr.co.heystars.api.account.repository.AccountRepository;
import kr.co.heystars.api.exception.InternalServerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Transactional
@Service
public class AuthService {

    @Autowired
    AccountRepository accountRepository;

    /*
    * google service key로 firebase 연동
    * */
    public AuthService(){

        try{
            ClassPathResource serviceAccount = new ClassPathResource("key/heystars-c4569-476ad7b7cf25.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount.getInputStream()))
                    .build();
            FirebaseApp.initializeApp(options);
        }catch (Exception e){
            log.info("{}", e.getMessage());
        }
    }


    /*
    * 유효한 firebase uid인지 체크
    * */
    public String verifiedUSer(String uid){

        String verifiedUserEmail = null;

        try{
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
            if(userRecord.isEmailVerified())
                verifiedUserEmail = userRecord.getEmail();
        }catch (Exception e){
            log.info("{}", e.getMessage());
        }

        return verifiedUserEmail;
    }

    /*
    * 유저정보 조회
    * */
    public HSAccount getUserInfo(String uid){
        try{
            String email = verifiedUSer(uid);
            if(email != null){
                HSAccount HSAccount = accountRepository.findByemail(email);
                return HSAccount;
            }else{
                return null;
            }
        }catch (Exception e){
            log.info("{}", e.getMessage());
        }

        return null;
    }

    /*
    * 유저 개인정보 저장
    * */
    public HttpStatus saveUserInfo(HSAccount HSAccount){

        System.out.println(HSAccount.toString());
        try{
            accountRepository.save(HSAccount);

        }catch (Exception e){
            e.printStackTrace();
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    /*
    유저 언어 정보 변경
    * */
    public HttpStatus putUserLang(String uid, HSAccount HSAccount){
        try{
            String email = verifiedUSer(uid);
            if(email != null){
                HSAccount hsAccount = accountRepository.findByemail(email);
                hsAccount.setLang(HSAccount.getLang());
//                log.info("hsAccount {}", hsAccount);
                accountRepository.save(hsAccount);
            }
        }catch (HttpStatusCodeException e){
            e.printStackTrace();
//            return HttpStatus.INTERNAL_SERVER_ERROR;
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "AuthService.putUserLang", e.getMessage());
        }
        return HttpStatus.OK;
    }
}
