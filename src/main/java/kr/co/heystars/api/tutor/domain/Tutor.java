package kr.co.heystars.api.tutor.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "Tutor")
public class Tutor {

    @Id
    @SequenceGenerator(name = "TUTOR_SEQ_GEN", sequenceName = "TUTOR_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TUTOR_SEQ_GEN")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "JOB")
    private String job;

    @Column(name = "MEMBER")
    private String member;

    @Column(name = "IMG_URL")
    private String imgUrl;
}
