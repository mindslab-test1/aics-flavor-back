package kr.co.heystars.api.tutor.service;

import kr.co.heystars.api.tutor.domain.Tutor;
import kr.co.heystars.api.tutor.repository.TutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TutorService {

    @Autowired
    TutorRepository tutorRepository;

    public List<Tutor> getTutorList(){

        return tutorRepository.findAll();
    }
}
