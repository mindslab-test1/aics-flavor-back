package kr.co.heystars.api.tutor.repository;

import kr.co.heystars.api.tutor.domain.Tutor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TutorRepository extends JpaRepository<Tutor,Long> {

}
