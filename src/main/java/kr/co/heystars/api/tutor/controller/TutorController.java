package kr.co.heystars.api.tutor.controller;

import kr.co.heystars.api.tutor.domain.Tutor;
import kr.co.heystars.api.tutor.service.TutorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tutor")
public class TutorController {

    private TutorService tutorService;

    TutorController(TutorService tutorService){this.tutorService = tutorService;}

    @ResponseBody
    @GetMapping(value = "/getTutors")
    public ResponseEntity getTutorList(){

        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(tutorService.getTutorList(), resHeaders, HttpStatus.OK);
    }
}
