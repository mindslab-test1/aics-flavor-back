package kr.co.heystars.api.stt.vo.response;

import lombok.Data;

@Data
public class SttRes {
    String status;
    String data;
}
