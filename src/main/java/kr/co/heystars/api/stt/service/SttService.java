package kr.co.heystars.api.stt.service;

import kr.co.heystars.api.exception.InternalServerException;
import kr.co.heystars.api.mediaServer.vo.response.MediaUploadRes;
import kr.co.heystars.api.stt.vo.response.SttRes;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

@Slf4j
@Service
public class SttService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${maum.api.id}")
    String id;

    @Value("${maum.api.key}")
    String key;

    @Value("${maun.stt.url}")
    String url;

    @Value("${maum.stt.lang}")
    String lang;

    @Value("${maum.stt.level}")
    String level;

    @Value("${maum.stt.sampling}")
    String sampling;

    @Value("${maum.stt.cmd}")
    String cmd;

    public SttRes sttCall(MultipartFile file){
        log.info("This is url {}", url);

        final URI sttUri = URI.create(url);

        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", file.getResource());
            body.add("ID", id);
            body.add("key", key);
            body.add("cmd", cmd);
            body.add("lang", lang);
            body.add("sampling", sampling);
            body.add("level", level);

            HttpEntity<MultiValueMap<String, Object>> requestHttpEntity = new HttpEntity<>(body, headers);
            ResponseEntity<SttRes> response = restTemplate.exchange(sttUri, HttpMethod.POST,
                    requestHttpEntity, new ParameterizedTypeReference<SttRes>() {});

            if(response.getStatusCode().value() == 200){
                SttRes sttRes = response.getBody();
                if("Success".equals(sttRes.getStatus()))
                    return sttRes;
                else
                    throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "SttService.sttCall", "Stt response status:"+sttRes.getStatus());
            }else{
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "SttService.sttCall", "Stt api call fail:"+response.getStatusCode().value());
            }

        }catch (HttpStatusCodeException e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "SttService.sttCall", e.getMessage());
        }
    }
}
