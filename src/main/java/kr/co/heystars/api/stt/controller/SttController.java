package kr.co.heystars.api.stt.controller;

import kr.co.heystars.api.stt.service.SttService;
import kr.co.heystars.api.stt.vo.response.SttRes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/stt")
public class SttController {

    @Autowired
    SttService sttService;

    @ResponseBody
    @PostMapping(value = "/maumStt")
    public SttRes MaumRestApi(@RequestParam("file") MultipartFile file){

        return sttService.sttCall(file);
    }
}
