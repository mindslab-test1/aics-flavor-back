package kr.co.heystars.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;


@Slf4j
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate(){
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(50)
                .setMaxConnPerRoute(20)
                .build();

        factory.setHttpClient(client);
        factory.setConnectTimeout(120 * 1000);
        factory.setReadTimeout(120 * 1000);
        factory.setBufferRequestBody(false);

        RestTemplate restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(factory));

        return restTemplate;
    }

    class LoggingInterceptor implements ClientHttpRequestInterceptor {

        @Override
        public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] body, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {

            log.debug("@ request Method >>> {}", httpRequest.getMethod());

            ClientHttpResponse response = clientHttpRequestExecution.execute(httpRequest, body);

            log.debug("@ Response Status code >>> {}", response.getStatusCode());

            return response;
        }
    }

}
