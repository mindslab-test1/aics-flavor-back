package kr.co.heystars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeystarsApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeystarsApplication.class, args);
    }

}
